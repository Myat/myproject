<?php
class PostsController extends AppController {
    public $helpers = array('Html', 'Form', 'Flash', 'Paginator');
    public $components = array('Flash', 'Paginator');

    public function index() {
        $this->set('posts', $this->Post->find('all'));
        $this->Paginator->settings = array(
            'Post' => array(
                'paramType' => 'querystring',
                'limit' => 5,
                'order' => array(
                   'Post.id' => 'asc'
                )
            )
        );

        $this->set('posts', $this->Paginator->paginate());
        
        $name = $this->Auth->user('username');
        $id = $this->Auth->user('id');
        $photo = $this->Auth->user('photo');
        
        $this->set('username', $name);
        $this->set('userid', $id);
        $this->set('photo', $photo);        
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);

        // for comment edit link
        $name = $this->Auth->user('username');
        $this->set('username', $name);

        $photo = $this->Auth->user('photo');
        $this->set('photo', $photo);

        //////////
        // save the comment
        if (!empty($this->data['Comment'])) {
            $this->request->data['Comment']['username'] = $this->Auth->user('username'); 
            $this->request->data['Comment']['post_id'] = $id;  
            $this->Post->Comment->create();


            //Check if image has been photoed
            if (!empty($this->request->data['Comment']['Photo']['name'])) {
                $file = $this->request->data['Comment']['Photo'];

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif','png');

                if (in_array($ext, $arr_ext)) {
                    if( move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $file['name']) ){
                       //prepare the filename for database entry
                        $this->request->data['Comment']['image_url'] = '/img/upload/' . $file['name']; 
                    } else {
                        $this->Flash->error(__('Unable to add your image.'));
                    }
                                        
                }
            }


            if ($this->Post->Comment->save($this->data)) {
                $this->Flash->success(__('Your comment has been saved.'));
                return $this->redirect(array('action' => 'view',$id));
            }
            $this->Flash->error(__('Unable to add your comment.'));
        }

        // set the view variables
        $post = $this->Post->read(null, $id); // contains $post['Comments']
        $this->set(compact('post'));
    
    }


    public function add() {
        if ($this->request->is('post')) {
           $this->Post->create();
            //Added this line
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');


            //Check if image has been photoed
            if (!empty($this->request->data['Post']['image']['name'])) {
                $file = $this->request->data['Post']['image'];

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif','png');

                if (in_array($ext, $arr_ext)) {
                    if( move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $file['name']) ){
                       //prepare the filename for database entry
                        $this->request->data['Post']['image'] = '/img/upload/' . $file['name']; 
                    } else {
                        $this->Flash->error(__('Unable to add your image.'));
                    }
                                        
                }
            }else{
                $this->request->data['Post']['image'] = null;
            }

            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
    }

    public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid post'));
	    }

	    $post = $this->Post->findById($id);

	    if (!$post) {
	        throw new NotFoundException(__('Invalid post'));
	    }

	    if ($this->request->is(array('post', 'put'))) {
	        $this->Post->id = $id;

            //Check if image has been photoed
            if (!empty($this->request->data['Post']['image']['name'])) {
                $file = $this->request->data['Post']['image'];

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif','png');

                if (in_array($ext, $arr_ext)) {
                    if( move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $file['name']) ){
                       //prepare the filename for database entry
                        $this->request->data['Post']['image'] = '/img/upload/' . $file['name']; 
                    } else {
                        $this->Flash->error(__('Unable to add your image.'));
                    }
                                        
                }
                
            }else{
                $this->request->data['Post']['image'] = null;
            }


	        if ($this->Post->save($this->request->data)) {
	            $this->Flash->success(__('Your post has been updated.'));
	            return $this->redirect(array('action' => 'index'));
	        }
	        $this->Flash->error(__('Unable to update your post.'));
	    }

	    if (!$this->request->data) {
	        $this->request->data = $post;
	    }
	}

	public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
                __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }


    public function isAuthorized($user) {
        // All registered users can add posts
        if ($this->action === 'add') {
            return true;
        }

        // The owner of a post can edit and delete it
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }


    public function search() {

        $key = $this->request->data['Post']['search'];

        if( $key != '') {
               $cond=array(
                'conditions' => array(
                    'OR'=>array('Post.id LIKE' => '%'. $key . '%',
                    'Post.title LIKE' => '%'. $key . '%',
                    'Post.body LIKE' => '%'. $key . '%'
                    )
                )
            );

            $result = $this->Post->find('all',$cond);
            $this->set('results', $result);

            if( $result == null ){
                 $this->Flash->error(__('No items match your search..'));
            } 
        }
        else {
            $this->Flash->error(__('Please type you want to search..'));
            return $this->redirect($this->Auth->redirectUrl());
        }

        
    }

}