<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    
    public function index() {
        $this->User->recursive = 0;
        $this->set('posts', $this->paginate());
        return $this->redirect($this->Auth->redirectUrl());
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();

            //Check if image has been photoed
            if (!empty($this->request->data['User']['photo']['name'])) {
                $file = $this->request->data['User']['photo'];

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif','png');

                if (in_array($ext, $arr_ext)) {
                    if( move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $file['name']) ){
                       //prepare the filename for database entry
                        $this->request->data['User']['photo'] = '/img/upload/' . $file['name']; 
                    }
                                        
                }
            }

            if ($this->User->save($this->request->data)) {                
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'login'));
            } else {
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
            
            $parentCategories = $this->Category->ParentCategory->find('list');
            $categoriesStatus = $this->Category->getCategoriesStatus();//model's method to get list of status
            $this->set(compact('parentCategories', 'categoriesStatus'));
        }
    }
    public function edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        // Prior to 2.5 use
         $this->request->onlyAllow('post');

        //$this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout');
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }



    public function logout() {
        return $this->redirect($this->Auth->logout());
    }


    public function register() {  
        if ($this->request->is('post')) { 
           $this->User->create();
               if ($this->User->save($this->request->data)) {
                     $this->Flash(__('New user registered'));
                     return $this->redirect(array('action' => 'login'));
                }
            $this->Flash(__('Could not register user'));
        }
    } 


}