<?php
class CommentsController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash', 'Paginator');
    public $components = array('Flash', 'Paginator');
    

    public function edit($id) {
        $this->Comment->id = $id;
        if (empty($this->data)) {
            $this->data = $this->Comment->read();
        } else {

            if (!empty($this->request->data['Comment']['image']['name'])) {
                $file = $this->request->data['Comment']['image'];

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif','png');

                if (in_array($ext, $arr_ext)) {
                    if( move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $file['name']) ){
                        //prepare the filename for database entry
                        $this->request->data['Comment']['image_url'] = '/img/upload/' . $file['name']; 
                    } else {
                        $this->Flash->error(__('Unable to add your image.'));
                    }
                                        
                }

            }else {
                $this->request->data['Comment']['image_url'] = null;
            }

            if ($this->Comment->save($this->data)) {
                $this->Flash->success(__('Your comment has been updated.'));
                $this->redirect(array('controller' => 'posts','action' => 'view',$this->data['Comment']['post_id']));
            }
        }
    }

    public function delete($id, $postid) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }


        if ($this->Comment->delete($id, true)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The post with id: %s could not be deleted.'));
        }

        $this->redirect(array('controller' => 'posts','action' => 'view',$postid));
    }

} 