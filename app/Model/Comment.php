
<?php

class Comment extends AppModel {

    var $name = 'Comment';
    var $belongsTo = array('Post'=>array('className'=>'Post'));

    public $validate = array(
        'comment' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Comment cannot be blank!')
        )
    );


}