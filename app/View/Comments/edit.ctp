<h1>Edit Comment</h1>

<?php

echo $this->Form->create('Comment', array('enctype'=>'multipart/form-data'));
echo $this->Form->input('comment');
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->input('post_id', array('type' => 'hidden'));
echo $this->Form->input('image', array('type'=>'file'));
echo $this->Form->end('Save Comment');
?>