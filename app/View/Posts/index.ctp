<!-- File: /app/View/Posts/index.ctp -->
<?php echo $this->Flash->render('auth'); ?>

<h1 align="right">


<?php 
    if( $photo != null ){
        echo $this->Html->image( $photo, array('alt' => 'CakePHP', 'width' => '50px', 'height' => '50px'));
    }
?>
<b><font color="blue"><?php echo $username; echo "\t"; ?></font></b>

<?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout')); ?>
</h1>

<h2>Blog posts</h2>


<p><?php echo $this->Html->link('Add Post', array('action' => 'add')); ?></p>


<?php
echo $this->Form->create('Post',array('url'=>array('controller'=>'posts','action'=>'search')));
echo $this->Form->input('search');
echo $this->Form->end(__('Search')); 
?>


<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php

                if ($userid == $post['Post']['user_id']) {
                    echo $this->Form->postLink(
                        'Delete',
                        array('action' => 'delete', $post['Post']['id']),
                        array('confirm' => 'Are you sure?')
                    );
                }
            ?>
            <?php
                if ($userid == $post['Post']['user_id']) {
                    echo $this->Html->link(
                        'Edit', array('action' => 'edit', $post['Post']['id'])

                    );
                }
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>
<div>
    <?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
</div>
<div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('Previous Page'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->next(__('Next Page') . ' >', array(), null, array('class' => 'next disabled'));
    ?>