<!-- File: /app/View/Posts/view.ctp -->
<?php echo $this->Html->link(' < Back to Home', array('action' => 'index')); ?>
<br><br>
<h2><?php echo $post['Post']['title']; ?></h2>

<small>Created: <?php echo $post['Post']['created']; ?></small>
<br><br>
<p>
<?php if( $post['Post']['image'] != null ) {
            echo $this->Html->image( $post['Post']['image'], array(
            'alt' => 'CakePHP', 'width' => '350px', 'height' => '300px'));

        }
?>
</p>

<p>
<?php echo h($post['Post']['body']); ?>
</p>

<br>
<?php if ( !empty($post['Comment']) ): ?>
		<h4 style="color:blue;">Comment</h4>
        <table>
            <?php foreach ($post['Comment'] as $comment): ?>
            <tr>
                <td>
                    <b><?php echo $comment['username']; ?></b>
                    <?php echo $comment['comment']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php
                            if ($username == $comment['username']) {
                                echo $this->Html->link(
                                    'Edit', array(
                                        'controller'=>'comments','action'=>'edit',$comment['id'])
                                );

                                echo "\t";

                                echo $this->Form->postLink(
                                    'Delete',
                                    array('controller'=>'comments','action' => 'delete',$comment['id'],$comment['post_id']),
                                    array('confirm' => 'Are you sure?')
                                );
                            }
                        ?>
                    <br>
                    <?php 
                        if( $comment['image_url'] != null ) {
                               echo $this->Html->image( 
                                                        $comment['image_url'],
                                                        array('alt' => 'CakePHP',
                                                              'width' => '100px',  
                                                              'height' => '100px'
                                                        )
                                ); 
                            } 
                        ?>
                        
                    </td>
            </tr>
            <?php endforeach; ?>
        </table>
<?php else: ?>
        <p><h4>No comments...</h4></p>
<?php endif; ?>

<br>
<p>
<?php
echo $this->Form->create('Comment',array('enctype'=>'multipart/form-data', 'url'=>array(
                                                                                'controller'=>'posts',
                                                                                'action'=>'view',
                                                                                $post['Post']['id'])));
echo $this->Form->input('comment', array( 'rows' => '1'));
echo $this->Form->input('Photo', array('type'=>'file'));
echo $this->Form->end(__('Add Comment')); 
?>
</p>

<?php echo $this->Html->link(' < Back to Home', array('action' => 'index')); ?>